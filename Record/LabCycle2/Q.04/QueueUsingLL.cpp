#include "QueueUsingLL.h"

Node :: Node(int data){
	this->data=data;
	this->link=NULL;
}

template<class T>Queue <T>:: Queue(int capacity){
	this->front=NULL;
	this->rear=NULL;
	this->capacity=capacity;
	this->size=0;
}

template<class T>bool Queue <T>:: isEmpty(){
	return size==0;
}

template<class T> bool Queue <T>:: isFull(){
	return size==capacity;
}

template<class T> void Queue <T>:: enqueue (int value){
	if(isFull()){
		cout<<"  Queue Overflow!"<<endl;
		return;
	}
	Node* newnode = new Node(value);
	if(rear==NULL){
		front = newnode;
	}
	else{
		rear->link=newnode;
	}
	rear = newnode;
	size++;
}

template<class T> int Queue <T> :: dequeue(){
	if(isEmpty()){
		cout<<"  Queue Underflow! "<<endl;
		return -1;
	}
	int removedata=front->data;
	front = front->link;
	if(front==NULL){
		rear =NULL;
	}
	size--;
	return removedata;
}

template<class T> void Queue <T>:: display(){
	if(isEmpty()){
		cout<<"  Queue is empty."<<endl;
		return;
	}
	Node* current= front;
	while(current != NULL){
		cout<<current->data<<" ";
		current= current->link;
	}
	cout<<endl;
}
