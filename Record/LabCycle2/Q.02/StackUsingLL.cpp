#include "StackUsingLL.h"

Node  :: Node(int value){
	data = value;
	link = NULL;
}

template<class T> Stack <T> :: Stack(){
	top = NULL;
}

template<class T> bool Stack <T> :: isEmpty(){
	return top == NULL;
}

template<class T> void Stack <T> :: push(T value){
	Node* newNode= new Node(value);
	newNode->link =top;
	top = newNode;
	cout<<"  pushed "<<value<<" onto the stack. "<<endl;
}

template<class T> int Stack <T> :: pop(){
	if(isEmpty()){
		cout<<"  Underflow! Cannot pop from an empty stack. "<<endl;
		return -1;
	}
	else{
		int poppedvalue = top->data;
		top = top->link;
		cout<<"  Popped "<<poppedvalue<<" from the stack "<<endl;
		return poppedvalue;
	}
}

template<class T> void Stack <T> :: display(){
	if(isEmpty()){
		cout<<"  Stack is Empty. "<<endl;
	}
	else{
		cout<<"  Elements in the Stack: ";
		Node* current = top;
		while(current != NULL){
			cout<<current->data<<" ";
			current = current->link;
		}
		cout<<endl;
	}
}
