#include<iostream>
using namespace std;

class Node{
	public:
		int data;
		Node* link;
		Node(int);
};

template<class T>
class Stack{
	private:
		Node* top;
	public:
		Stack();
		bool isEmpty();
		void push(T);
		int pop();
		void display();
};
