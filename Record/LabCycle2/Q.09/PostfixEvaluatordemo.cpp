#include "PostfixEvaluator.cpp"
int main() {
    string exp;
    cout << "Enter the postfix expression: ";
    cin >> exp;
    cout << "Result: " << evaluatePostfix(exp) << endl;
    return 0;
}
