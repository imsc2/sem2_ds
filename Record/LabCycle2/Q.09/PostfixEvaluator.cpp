#include "PostfixEvaluator.h"
#include <iostream>
#include <string>
using namespace std;

template <class T>
Stack<T>::Stack(int size) {
    capacity = size;
    arr = new T[capacity];
    top = -1;
}

template <class T>
Stack<T>::~Stack() {
    delete[] arr;
}

template <class T>
void Stack<T>::push(T item) {
    if (isFull()) {
        throw std::overflow_error("Stack is full");
    }
    arr[++top] = item;
}

template <class T>
T Stack<T>::pop() {
    if (isEmpty()) {
        throw std::underflow_error("Stack is empty");
    }
    return arr[top--];
}

template <class T>
T Stack<T>::peek() {
    if (isEmpty()) {
        throw std::underflow_error("Stack is empty");
    }
    return arr[top];
}

template <class T>
bool Stack<T>::isEmpty() {
    return top == -1;
}

template <class T>
bool Stack<T>::isFull() {
    return top == capacity - 1;
}
int evaluatePostfix(string exp) {
    Stack<int> stack(exp.length());

    for (char& c : exp) {
        if (isdigit(c)) {
            stack.push(c - '0');
        } else {
            int val2 = stack.pop();
            int val1 = stack.pop();
            switch (c) {
                case '+': stack.push(val1 + val2); break;
                case '-': stack.push(val1 - val2); break;
                case '*': stack.push(val1 * val2); break;
                case '/': stack.push(val1 / val2); break;
            }
        }
    }
    return stack.pop();
}
