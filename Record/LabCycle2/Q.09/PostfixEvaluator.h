#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <stdexcept>

template <class T>
class Stack {
private:
    T* arr;
    int capacity;
    int top;

public:
    Stack(int size);
    ~Stack();

    void push(T item);
    T pop();
    T peek();
    bool isEmpty();
    bool isFull();
};

#endif

