#include "MusicPlaylist.cpp"

int main() {
    Playlist playlist;

    playlist.insertSong("Thaniye", "Vishnu Vijay", 300, "Melody");
    playlist.insertSong("Kattu mooliyo", "Vineeth Sreenivasan", 230, "Melody");
    playlist.insertSong("vaa vathi", "Swetha mohan", 230, "Melody");
    playlist.insertSong("Dynamite", "BTS", 200, "Pop");
    playlist.insertSong("Chandanamani", "M G Sreekumar", 190, "Rap");

    cout << "Original Playlist:\n";
    playlist.displayPlaylist();

    cout << "\nSorted Playlist by Title:\n";
    playlist.sortByTitle();
    playlist.displayPlaylist();

    cout << "\nSorted Playlist by Artist:\n";
    playlist.sortByArtist();
    playlist.displayPlaylist();

    cout << "\nSorted Playlist by Duration:\n";
    playlist.sortByDuration();
    playlist.displayPlaylist();

    cout << "\nSorted Playlist by Genre:\n";
    playlist.sortByGenre();
    playlist.displayPlaylist();

    return 0;
}
