#include "DoublyLL.h"

template<typename T>
DoublyLinkedList<T>::DoublyLinkedList() : head(NULL), tail(NULL) {}

template<typename T>
DoublyLinkedList<T>::~DoublyLinkedList() {
    while (head != NULL) {
        Node<T>* temp = head;
        head = head->q;
        delete temp;
    }
}

template<typename T>
void DoublyLinkedList<T>::insertAtBeginning(T a) {
    Node<T>* newNode = new Node<T>;
    newNode->a = a;
    newNode->p = NULL;
    newNode->q = head;

    if (head != NULL) {
        head->p = newNode;
    } else {
        tail = newNode;
    }

    head = newNode;
}

template<typename T>
void DoublyLinkedList<T>::insertAtEnd(T a) {
    Node<T>* newNode = new Node<T>;
    newNode->a = a;
    newNode->q = NULL;
    if (head == NULL) {
        head = tail = newNode;
        newNode->p = NULL;
    } else {
        tail->q = newNode;
        newNode->p = tail;
        tail = newNode;
    }
}

template<typename T>
void DoublyLinkedList<T>::insertAtPosition(int pos, T a) {
    if (pos < 0) {
        cout << "Invalid position. Insertion failed." << endl;
        return;
    }

    if (pos == 0) {
        insertAtBeginning(a);
    } else {
        Node<T>* newNode = new Node<T>;
        newNode->a = a;
        Node<T>* current = head;
        int count = 0;
        while (current != NULL && count < pos - 1) {
            current = current->q;
            ++count;
        }
        if (current == NULL) {
            cout << "Invalid position. Insertion failed." << endl;
            delete newNode;
            return;
        }

        newNode->q = current->q;
        newNode->p = current;
        if (current->q != NULL) {
            current->q->p = newNode;
        } else {
            tail = newNode;
        }
        current->q = newNode;
    }
}

template<typename T>
void DoublyLinkedList<T>::deleteFromBeginning() {
    if (head == NULL) {
        cout << "List is empty. Deletion failed." << endl;
        return;
    }

    Node<T>* temp = head;
    head = head->q;
    if (head != NULL) {
        head->p = NULL;
    } else {
        tail = NULL;
    }
    delete temp;
}

template<typename T>
void DoublyLinkedList<T>::deleteFromEnd() {
    if (head == NULL) {
        cout << "List is empty. Deletion failed." << endl;
        return;
    }

    if (head == tail) {
        delete head;
        head = tail = NULL;
    } else {
        Node<T>* temp = tail;
        tail = tail->p;
        tail->q = NULL;
        delete temp;
    }
}

template<typename T>
void DoublyLinkedList<T>::deleteFromPosition(int pos) {
    if (pos < 0 || head == NULL) {
        cout << "Invalid position. Deletion failed." << endl;
        return;
    }

    if (pos == 0) {
        deleteFromBeginning();
    } else {
        Node<T>* current = head;
        int count = 0;

        while (current != NULL && count < pos) {
            current = current->q;
            ++count;
        }

        if (current == NULL) {
            cout << "Invalid position. Deletion failed." << endl;
            return;
        }

        if (current == tail) {
            tail = tail->p;
            tail->q = NULL;
        } else {
            current->p->q = current->q;
            current->q->p = current->p;
        }

        delete current;
    }
}

template<typename T>
void DoublyLinkedList<T>::display() {
    Node<T>* current = head;
    while (current != NULL) {
        cout << current->a << " ";
        current = current->q;
    }
    cout << endl;
}
