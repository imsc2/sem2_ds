#include "LinkedList.h"

template<typename T>
LinkedList<T>::LinkedList() : head(NULL) {}

template<typename T>
LinkedList<T>::~LinkedList() {
    Node<T>* current = head;
    while (current != NULL) {
        Node<T>* next = current->next;
        delete current;
        current = next;
    }
    head = NULL;
}

template<typename T>
void LinkedList<T>::insertAtBeginning(const T& p) {
    Node<T>* newNode = new Node<T>(p);
    newNode->next = head;
    head = newNode;
}

template<typename T>
void LinkedList<T>::insertAtEnd(const T& p) {
    Node<T>* newNode = new Node<T>(p);
    if (head == NULL) {
        head = newNode;
        return;
    }
    Node<T>* current = head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

template<typename T>
void LinkedList<T>::insertAtPosition(int pos, const T& p) {
    if (pos < 0) {
        cout << "Invalid position. Insertion failed." << endl;
        return;
    }
    if (pos == 0) {
        insertAtBeginning(p);
        return;
    }
    Node<T>* newNode = new Node<T>(p);
    Node<T>* current = head;
    for (int i = 0; i < pos - 1 && current != NULL; ++i) {
        current = current->next;
    }
    if (current == NULL) {
        cout << "Invalid position. Insertion failed." << endl;
        return;
    }
    newNode->next = current->next;
    current->next = newNode;
}

template<typename T>
void LinkedList<T>::deleteFromBeginning() {
    if (head == NULL) {
        cout << "List is empty. Cannot delete from beginning." << endl;
        return;
    }
    Node<T>* temp = head;
    head = head->next;
    delete temp;
}

template<typename T>
void LinkedList<T>::deleteFromEnd() {
    if (head == NULL) {
        cout << "List is empty. Cannot delete from end." << endl;
        return;
    }
    if (head->next == NULL) {
        delete head;
        head = NULL;
        return;
    }
    Node<T>* current = head;
    while (current->next->next != NULL) {
        current = current->next;
    }
    delete current->next;
    current->next = NULL;
}

template<typename T>
void LinkedList<T>::deleteFromPosition(int pos) {
    if (head == NULL || pos < 0) {
        cout << "Invalid position. Deletion failed." << endl;
        return;
    }
    if (pos == 0) {
        deleteFromBeginning();
        return;
    }
    Node<T>* current = head;
    for (int i = 0; i < pos - 1 && current->next != NULL; ++i) {
        current = current->next;
    }
    if (current->next == NULL) {
        cout << "Invalid position. Deletion failed." << endl;
        return;
    }
    Node<T>* temp = current->next;
    current->next = current->next->next;
    delete temp;
}

template<typename T>
void LinkedList<T>::display() {
    if (head == NULL) {
        cout << "List is empty." << endl;
        return;
    }
    Node<T>* current = head;
    cout << "List elements: ";
    while (current != NULL) {
        cout << current->data << " ";
        current = current->next;
    }
    cout << endl;
}
