#include<iostream>
using namespace std;
class matrix{
	public:
		int matrix1[3][3];
		int i,j,k;
		friend istream & operator >>(istream & is , matrix & m){
			for(m.i=0;m.i<3;m.i++){
				for(m.j=0;m.j<3;m.j++){
					is>>m.matrix1[m.i][m.j];
				}
				cout<<endl;
			}
			return is;
		}
		friend ostream & operator << (ostream & os, matrix & m){
			for(m.i=0;m.i<3;m.i++){
				for(m.j=0;m.j<3;m.j++){
					os<<m.matrix1[m.i][m.j]<<" ";
				}
				os<<endl;
			}
			return os;
		}
		friend matrix operator + (matrix m1,matrix m2){
			matrix sum;
			for(sum.i=0;sum.i<3;sum.i++){
				for(sum.j=0;sum.j<3;sum.j++){
					sum.matrix1[sum.i][sum.j]=m1.matrix1[sum.i][sum.j]+m2.matrix1[sum.i][sum.j];
				}
			}
			return sum;
		}
		friend matrix operator - (matrix m1,matrix m2){
			matrix sub;
			for(sub.i=0;sub.i<3;sub.i++){
				for(sub.j=0;sub.j<3;sub.j++){
					sub.matrix1[sub.i][sub.j]=m1.matrix1[sub.i][sub.j]-m2.matrix1[sub.i][sub.j];
				}
			}
			return sub;
		}
		friend matrix operator * (matrix m1, matrix m2){
			matrix mult;
			for(mult.i=0;mult.i<3;mult.i++){
				for(mult.j=0;mult.j<3;mult.j++){
					mult.matrix1[mult.i][mult.j]=0;
					for(mult.k=0;mult.k<3;mult.k++){
						mult.matrix1[mult.i][mult.j]+=m1.matrix1[mult.i][mult.k]*m2.matrix1[mult.k][mult.j];
					}
				}
			}
			return mult;
		}
};
