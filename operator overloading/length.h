#include<iostream>
using namespace std;
class Metric{
	public:
		float m,cm;
		friend istream & operator >>(istream & is , Metric met){
			cout<<"meter = ";
			is>>met.m;
			cout<<"centemeter = ";
			is>>met.cm;
			return is;
		}
		Metric operator = (Metric & met);
		
};
class Imperial{
	public:
		float ft,in;
		friend istream & operator >>(istream & is , Imperial imp){
			cout<<"feet = ";
			is>>imp.ft;
			cout<<"inch = ";
			is>>imp.in;
			return is;
		}
		Imperial operator = (Imperial & imp);
};

Metric operator = (Metric & met){
	Imperial imp;
	met.cm=met.m*100+met.cm;
	imp.ft=static_cast<int>(met.cm*0.0328084);
	imp.in=(met.cm*0.393701)-(imp.m*12);
	return imp;
}
