#include<iostream>
using namespace std;
class complex{
	float real,img;
	public:
		complex(){
			real=0;
			img=0;
		}
		complex(float r,float i){
		    this->real=r;
			this->img=i;
		}
		friend istream & operator >> (istream & is, complex & c){
		    cout<<"The value of real number : ";
		    is>>c.real;
		    cout<<"The value of imaginary number : ";
		    is>>c.img;
		    return is;
	    }
		friend ostream & operator <<(ostream & os , complex & c){
		    if (c.img<0){
			    os<<c.real<<c.img<<"i"<<endl;
		    }
		    else{
			    os<<c.real<<"+"<<c.img<<"i"<<endl;
		    }
		    return os;
	    }
		complex operator + (complex & obj){
		    complex sum;
		    sum.real=this->real+obj.real;
		    sum.img=this->img+obj.img;
		    return sum;
	    }
		friend complex operator * (complex a,complex b){
		    complex product;
		    product.real=a.real*b.real-a.img*b.img;
		    product.img=a.real*b.img+a.img*b.real;
		    return product;
	    }
	    friend complex operator - (complex & obj){
	    	complex complement;
	    	complement.real=obj.real;
	    	complement.img=obj.img*-1;
	    	return complement;
		}
	
};
