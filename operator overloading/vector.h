#include<iostream>
using namespace std;
class vector{
	public:
		float x;
		float y;
		float z;
		friend istream & operator >>(istream & is , vector & obj){
			cout<<"x co-ordinate =";
			is>>obj.x;
			cout<<"y co-ordinate =";
			is>>obj.y;
			cout<<"z co-ordinate = ";
			is>>obj.z;
			return is;
		}
		friend ostream & operator <<(ostream & os , vector & obj){
			os<<obj.x<<"i ";
			if(obj.y<0){
				os<<obj.y<<"j "; 
			}
			else{
				os<<"+ "<<obj.y<<"j ";
			}
			if(obj.z<0){
				os<<obj.z<<"k"<<endl;
			}
			else{
				os<<"+ "<<obj.z<<"k"<<endl;
			}
			
			return os;
		}
		friend vector operator + (vector v1, vector v2){
			vector sum;
			sum.x=v1.x+v2.x;
			sum.y=v1.y+v2.y;
			sum.z=v1.z+v2.z;
			return sum;
		}
		friend vector operator - (vector v1,vector v2){
			vector sub;
			sub.x=v1.x-v2.x;
			sub.y=v1.y-v2.y;
			sub.z=v1.z-v2.z;
			return sub;
		}
		friend vector operator ^ (vector v1,vector v2){
			vector dot;
			dot.x=v1.x*v2.x;
			dot.y=v1.y*v2.y;
			dot.z=v1.z*v2.z;
			cout<<"Dot product of vectors ="<<dot.x+dot.y+dot.z<<endl;
			return dot;
		}
		friend vector operator * (vector v1, vector v2){
			vector cross;
			cross.x=v1.y*v2.z-v1.z*v2.y;
			cross.y=v1.z*v2.x-v1.x*v2.z;
			cross.z=v1.x*v2.y-v1.y*v2.x;
			return cross;
		}
		
};
