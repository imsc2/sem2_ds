#include <iostream>
using namespace std;

class Node{
	int data;
	Node* link;
	
	public:
		Node(int);
		int get_data();
		void set_data(int);
		Node* get_link();
		void set_link(Node*);
};

class Linkedlist{
	private:
		Node* head;
		
	public:
		Linkedlist();
		void create_LL();
		void traversal();
		void insert_at_beginning(int);
		void insert_at_end(int);
		void insert_after_key(int,int);
		void insert_before_key(int,int);
		void delete_at_begin();
		void delete_at_end();
		void delete_key(int);
		void concat_LL(Linkedlist);
		Node* search(int);
		void reverse_list();
		void reverse_traversal();
		void sort();
		Node* find_min(Node*) const;
};
