#include "linkedlist.h"

Node :: Node(int data){
	this->data=data;
	this->link=NULL;
}

int Node :: get_data(){
	return data;
} 

void Node :: set_data(int data){
	this->data=data;
}

Node* Node :: get_link(){
	return link;
}

void Node :: set_link(Node* link){
	this->link=link;
}


Linkedlist :: Linkedlist(){
	head=NULL;
}

void Linkedlist :: create_LL(){
	char option;
	int x;
	Node* current;
	current=head;
	
	do{
		cout<<"Enter the value : ";
		cin>>x;
		Node* node =new Node(x);
		if(head==NULL){
			head=node;
			current=head;
		}
		else{
			current->set_link(node);
			current=node;
		}

		
		cout<<"Do you want to add number (y/n) : ";
		cin>>option;
	}
	while(option != 'n');
}

void Linkedlist :: traversal(){
	Node* p=head;
		while (p!=NULL){
			cout<<p->get_data()<<endl;
			p=p->get_link();
		}

	cout<<endl;
}

void Linkedlist :: insert_at_beginning(int key){
	Node* node= new Node(key);
	node->set_data(key);
	node->set_link(head);
	head=node;
}

void Linkedlist :: insert_at_end(int key){
	Node* current=head;
	while(current->get_link()!=NULL){
		current=current->get_link();
	}
	Node* node = new Node(key);
	current->set_link(node);
}

void Linkedlist::insert_after_key(int key,int item){
	Node* p=head;
	while(p->get_link()!= NULL && p->get_data()!= key){
		p=p->get_link();
	}
	if(p== NULL){
		cout<<"Key not found in list "<<endl;
		return ;
	}
	Node* q=p->get_link();
	Node* node= new Node(item);
	node->set_data(item);
	node->set_link(q);
	p->set_link(node);	
}

void Linkedlist :: insert_before_key(int key, int item){
	if (head == NULL) {
        cout << "List is empty." << endl;
        return;
    }
    if (head->get_data() == key) {
        insert_at_beginning(item);
        return;
    }
	Node* p = head->get_link();
	Node* q = p->get_link();
	while (q->get_data()!= key){
		p = q;
		q = q->get_link();
	}
	if (q == NULL) {
        cout << "Key not found in the list." << endl;
        return;
    }
    Node* node = new Node(item);
    node->set_link(q);
    p->set_link(node);
}

void Linkedlist::delete_at_begin(){
	if(head== NULL){
		cout<<"List is empty"<<endl;
		return;
	}
		Node* p= head;
		head= head->get_link();
		delete p;			
		
}

void Linkedlist::delete_at_end(){
	Node* p =head;
	Node* q= p->get_link();
	while(q->get_link() != NULL ){
		p=q;
		q=q->get_link();
	}
	delete p->get_link();
	p->set_link(NULL);
}

void Linkedlist::delete_key(int key){
	Node* p=head;
	Node* q=p->get_link();
	while(q->get_data() != key){
		p=q;
		q=q->get_link();
	}
	p->set_link(q->get_link());
	delete q;
}

void Linkedlist::concat_LL(Linkedlist l1){
	if(head== NULL){
		head=l1.head;
		return;
	}
	Node* current= head;
	while(current->get_link() != NULL){
		current = current->get_link();
	}
	current->set_link(l1.head);
}	

Node* Linkedlist :: search(int key){
	Node* current = head;
	while(current->get_link()!= NULL){
		if(current->get_data ()==key){
			cout<<"Found : "<<current->get_data()<<endl;
			return current;
		}
		else{
			cout<<"Cannot Found key"<<endl;
			return current;
		}
		current=current->get_link();
	}
	return NULL;
} 

void Linkedlist::reverse_list(){
	Node* p=NULL;
	Node* current=head;
	Node* next=NULL;
	
	while(current != NULL){
		next=current->get_link();
		current->set_link(p);
		p=current;
		current=next;
	}
	head=p;
}

void Linkedlist::reverse_traversal(){
	Node* current = head;
    Node* previous = NULL;
    Node* next = NULL;

    while (current != NULL) {
        next = current->get_link();  
        current->set_link(previous); 
        previous = current;
        current = next;
    }

    current = previous; 

    while (current != NULL) {
        cout << current->get_data() <<endl;
        current = current->get_link();
    }
}

Node* Linkedlist::find_min(Node* start) const {
    if (start == NULL) {
        return NULL;
    }

    Node* minNode = start;
    Node* current = start->get_link();

    while (current != NULL) {
        if (current->get_data() < minNode->get_data()) {
            minNode = current;
        }
        current = current->get_link();
    }

    return minNode;
}

void Linkedlist::sort() {
    if (head == NULL || head->get_link() == NULL) {
        cout << "List is already sorted or empty. Nothing to sort." << endl;
        return;
    }

    Node* current = head;

    while (current != NULL) {
        Node* minNode = find_min(current);

        int temp = current->get_data();
        current->set_data(minNode->get_data());
        minNode->set_data(temp);

        current = current->get_link();
    }
}


