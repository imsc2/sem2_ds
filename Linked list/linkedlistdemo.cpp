#include"linkedlist.cpp"

int main(){
	Linkedlist l1;
	l1.create_LL();
	cout<<"Created Linked list 1 "<<endl;
	l1.traversal();
	
	l1.insert_at_beginning(3);
	l1.insert_at_beginning(5);
	l1.insert_at_end(10);
	l1.insert_at_end(20);
	cout<<"Linked list after insertion at beginning and end "<<endl;
	l1.traversal();
	
	l1.delete_at_begin();
	l1.delete_at_end();
	cout<<"Linked List after deletion at beginning and end "<<endl;
	l1.traversal();
	
	cout<<"Search\n";
	int s;
	cout<<"Enter the value for search : ";
	cin>>s;
	l1.search(s);
	
	cout<<"Reverse\n";
	l1.reverse_list();
	l1.traversal();
	
	cout<<"Create Linked List 2 \n";
	Linkedlist l2;
	l2.create_LL();
	cout<<"Created list 2 "<<endl;
	l2.traversal();
	
	l2.insert_after_key(3,2);
	l2.insert_before_key(9,5);
	cout<<"Linked List after insertion before and after keys\n";
	l2.traversal();
	
	cout<<"Delete\n";
	int del;
	cout<<"Enter the key to delete :";
	cin>>del;
	l2.delete_key(del);
	l2.traversal();
	
	cout<<"SORTING\n";
	cout<<"Sorted Linked List 1 :\n";
	l1.sort();
	l1.traversal();
	cout<<"Sorted Linked List 2 :\n";
	l2.sort();
	l2.traversal();
	
	cout<<"CONCATENATION\n";
	l1.concat_LL(l2);
	l1.traversal();
	
	cout<<"Reverse traversal of Linked List 2 "<<endl;
	l2.reverse_traversal();
	
	return 0;
}


