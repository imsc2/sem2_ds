#include"Stack.cpp"

int main(){
	Stack<int>mystack;
	cout<<"is stack empty : "<<mystack.isEmpty()<<endl;
	mystack.Push(5);
	mystack.Push(7);
	mystack.Push(8);
	mystack.Push(10);
	cout<<"is stack full : "<<mystack.isFull()<<endl;
	cout<<"Peak element of stack = "<<mystack.Peak()<<endl;
	mystack.Pop();
	cout<<"Peak element of stack after poping = "<<mystack.Peak()<<endl;
	cout<<"Status = "<<mystack.Status()<<endl;
	
	cout<<"********************************************"<<endl;
	
	int size,n;
	int s[100];
	cout<<"Enter the size of stack = ";
	cin>>size;
	
	cout<<"Enter the number of elements = ";
	cin>>n;
	
	cout<<"enter the elements : ";
	for(int i=0;i<n;i++){
		cin>>s[i];
	}
	
	Stack<int>my_stack(n-1,size,s);
	cout<<"is stack empty : "<<my_stack.isEmpty()<<endl;
	cout<<"is stack full : "<<my_stack.isFull()<<endl;
	my_stack.Pop();
	my_stack.Push(5);
	cout<<"Peak element of stack = "<<my_stack.Peak()<<endl;
	cout<<"Status : "<<my_stack.Status()<<endl;
	
	return 0;
}
