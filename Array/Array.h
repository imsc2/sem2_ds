#include<iostream>
#include<ostream>
using namespace std;
template <class T>
class Array{
	
	public:
		int LB,UB;
    	T A[100];
    	
    	
		Array();
		Array(int,int,T[]);
		void insert_at_end(T);
		void insert_at_beginning(T);
		void insert_at_index(T,int);
		void delete_at_end();
		void delete_at_beginning();
		void delete_at_index(int);
		T linear_search(T);
		T binary_search(T);
		void selection_sort();
		void bubble_sort();
		void swap(int,int);
		void insertion_sort();
		int partition(int,int);
		void quick_sort(int,int);
		void merge_sort(T[],T[],T[],int,int,int,int);
		void rotate_clockwise(int);
		void rotate_aniclockwise(int);
		void print_distinct_element();
		void generate_frequency_table();
		
		template <class U>
		friend ostream & operator << (ostream & os , Array<U> );
};
