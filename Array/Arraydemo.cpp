#include"array.cpp"
int main(){
	int a;
	Array<int>myArray;
	myArray.insert_at_end(3);
	myArray.insert_at_end(5);
	myArray.insert_at_end(6);
	cout<<"Array after insertion at the end = "<<myArray<<endl;
	
	myArray.insert_at_beginning(2);
	myArray.insert_at_beginning(3);
	myArray.insert_at_beginning(4);
	cout<<"Array after insertion at the beginning = "<<myArray<<endl;
	
	myArray.insert_at_index(20,3);
	myArray.insert_at_index(5,4);
	myArray.insert_at_index(3,6);
	cout<<"Array after insertion at the index = "<<myArray<<endl;
	
	myArray.delete_at_end();
	myArray.delete_at_beginning();
	myArray.delete_at_index(5);
	cout<<"Array after the deletion : "<<myArray<<endl;
	
	myArray.selection_sort();	
	cout<<"Array after selection sort = "<<myArray<<endl;
	myArray.bubble_sort();
	cout<<"Array after buble sorting = "<<myArray<<endl;
	
	cout<<"Enter a integer =";
	cin>>a;
	cout<<"Position of "<<a<<" in the array (binary search) = "<<myArray.binary_search(a)<<endl;
	cout<<"Position of "<<a<<" in the array (linear search) = "<<myArray.linear_search(a)<<endl;
	
	cout<<"*****************************************************************"<<endl;

	
	Array<float>myfArray;
	myfArray.insert_at_end(4.9);
	myfArray.insert_at_end(2.6);
	myfArray.insert_at_end(9.1);
	myfArray.insert_at_beginning(12.5);
	myfArray.insert_at_beginning(2.6);
	myfArray.insert_at_beginning(6.5);
	
	myfArray.selection_sort();
	cout<<"Array after selection sort : "<<myfArray<<endl;
	
	myfArray.insert_at_index(5.2,4);
	myfArray.insert_at_index(8.7,2);
	myfArray.insert_at_index(4.9,7);
	cout<<"Array after insertion at index : "<<myfArray<<endl;

	
	myfArray.delete_at_end();
	myfArray.delete_at_beginning();
	myfArray.delete_at_index(2.6);
	cout<<"Array after deletion : "<<myfArray<<endl;
	
	myfArray.quick_sort(myfArray.LB , myfArray.UB);
	cout<<"Array after Quick sorting : "<<myfArray<<endl;
	
/*	cout<<"Enter an number to search in the array : ";
	cin>>a;*/
	cout<<"Position of elemtnt in the Array(binary search) : "<<myfArray.binary_search(6.5)<<endl;
	cout<<"Position of element in the Array(linear search) : "<<myfArray.linear_search(8.7)<<endl;
	
	myfArray.generate_frequency_table();
/*	myfArray.rotate_clockwise(1);*/
	
	cout<<"Array : "<<myfArray<<endl;
	myfArray.print_distinct_element(); 

	
	return 0;
}

