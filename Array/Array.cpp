
#include"array.h"

template <class T>Array <T>::Array(){
	LB=1;
	UB=0;
}
template <class T> Array <T>::Array(int LB,int UB, T x[]){
	int i=LB;
	while(i<=UB){
		A[i]=x[i];
		i++;
	}
}

template <class T> void Array <T>::insert_at_end(T key){
	UB=UB+1;
	A[UB]=key;
}

template <class T> void Array <T>::insert_at_beginning(T key){
	UB=UB+1;
	int K=UB-1;
	while(K>=LB){
		A[K+1]=A[K];
		K-=1;
	}
	A[LB]=key;
}

template <class T> void Array <T>::insert_at_index(T key,int pos){
	UB+=1;
	int K=UB-1;
	while(K>=pos){
		A[K+1]=A[K];
		K--;
	}
	A[pos]=key;
}

template <class T> void Array <T>::delete_at_end(){
	if(UB>=LB){
		UB-=1;
	}
	else{
		cout<<"No element in this array"<<endl;
	}
}

template <class T> void Array <T>::delete_at_beginning(){
	if(UB>=LB){
		for(int i=LB+1;i<=UB;i++){
			A[i-1]=A[i];
		}
		UB=UB-1;
	}
	else{
		cout<<"No element in this array"<<endl;
	}
}

template <class T> void Array <T>::delete_at_index(int pos){
	if(LB<=pos and UB>=pos){
		int K=pos+1;
		while(K<=UB){
			A[K-1]=A[K];
			K+=1;
		}
		UB-=1;
	}
	else{
		cout<<"Out of bound"<<endl;
	}
}

template <class T> T Array <T>::linear_search(T key){
	int index=0;
	int i=LB;
	while(i<=UB){
		if(A[i]==key){
			index=i;
			break;
		}
		i+=1;
	}
	return index;
}

template <class T> T Array <T>::binary_search(T value){
	int a=LB;
	int b=UB;
	int mid=0;
	while(a<=b){
		mid=(a+b)/2;
		if(A[mid]==value){
			return mid;
		}
		else if(A[mid]>value){
			a=a;
			b=mid;
		}
		else if(A[mid]<value){
			b=b;
			a=mid+1;
		}
		
	}
	return -1;
	
}

template<class T>
void Array<T>::selection_sort(){
	for (int i=LB;i<UB;i++){
		int min=i;
		for(int j=i+1;j<=UB;j++){
			if(A[j]<A[min]){
				min=j;
			}
		}
		if(min!=i){
			swap(i,min);
		}
	}
}

template<class T>void Array<T>::swap(int p,int q){
	T t=A[p];
	A[p]=A[q];
	A[q]=t;
}

template<class T>
void Array<T>::bubble_sort(){
	for(int i=LB;i<=UB-1;i++){
		for(int j=LB;j<=UB+LB-i-1;j++){
			if(A[j]>A[j+1]){
				swap(j,j+1);
			}
		}
	}
}

template<class T>void Array<T>::insertion_sort(){
	int n = (UB-LB)+1;
	for(int i=1;i<n;i++){
		T key =A[i];
		int j=i-1;
		while(j>=0 && A[j]>key){
			A[j+1] = A[j];
			j = j-1;
		}
		A[j+1] = key;
	}
}

template<class T> int Array<T>::partition(int LB,int UB){
	T pivot = A[UB];
	int i = LB -1;
	for(int j = LB;j<UB;j++){
		if(A[j]<pivot){
			++i;
			swap(i,j);
		}
	}
	swap(i+1,UB);
	return(i+1);
}

template<class T>void Array<T>::quick_sort(int LB,int UB){
	if(LB<UB){
		int part_index = partition(LB,UB);
		quick_sort(LB,part_index-1);
		quick_sort(part_index+1,UB);
	}
}

/********************************************************************************/


template <class T>void Array<T>::merge_sort(T A[],T B[], T C[], int A_LB,int A_UB,int B_LB,int B_UB){
	int k=0;
	int i=A_LB;
	int j=B_LB;
	for(;i<=A_UB && j<=B_UB;k++){
		if(A[i]<B[j]){
			C[k] = A[i];
			i++;
		}
		else if (B[j]<A[i]){
			C[k] = B[j];
			j++;
		}
		else{
			C[k] = B[j++];
			C[++k] = A[i++];
		}
	}
	while(i<=A_UB){
		C[k++] = A[i++];
	}
	while(j<=B_UB){
		C[k++] = B[j++];
	}
}

template<class T>void Array<T>::rotate_clockwise(int k){
	int n =UB - LB +1;
	k = k % n ;
	if(k < 0){
		k+=n;
	}
	T* temp =new T[n];
	
	for(int i=0; i<n; ++i){
		temp[(i+k) % n] = A[i];
	}
	for(int i=0;i<n;++i){
		A[i] = temp[i];
	}
	delete[] temp;
}

template<class T>void Array<T>::rotate_aniclockwise(int k){
	int n =UB - LB +1;
	k = k % n ;
	if(k < 0){
		k+=n;
	}
	T* temp =new T[n];
	for(int i=0; i<n; ++i){
		temp[i] = A[(i+k) % n];
	}
	for(int i=0;i<n;++i){
		A[i] = temp[i];
	}
	delete[] temp;	
}

template<class T>void Array<T>::print_distinct_element(){
	int n = UB-LB+1;
	cout<<"Distinct Elements : ";
	for(int i=1;i<=n;++i){
		bool isDistinct = true;
		for(int j=0;j<i;++j){
			if(A[i] == A[j]){
				isDistinct =false;
				break;
			}
		}
		if(isDistinct){
			cout<<A[i]<<" ";
		}
	}
	cout<<endl;
}

template<class T>void Array<T>::generate_frequency_table(){
	int n = UB-LB+1;
	cout<<"Frequency Table : "<<endl;
	for(int i=1;i<=n;++i){
		int count=1;
		for(int j=i+1;j<n;++j){
			if(A[i]==A[j]){
				++count;
			}
		}
		bool isCounted = false;
		for(int k =0;k<i;++k){
			if(A[i] == A[k]){
				isCounted = true;
				break;
			}
		}
		if(!isCounted){
			cout<<A[i]<<" : "<<count<<endl;
		}
	}
}

/**********************************************************************************/

template <class U>ostream & operator <<(ostream & os, Array <U>M){
	int i;
	os<<endl;
	for(i=M.LB;i<=M.UB;i++){
		os<<M.A[i]<<" ";
	}
	os<<endl;
	return os;
}


